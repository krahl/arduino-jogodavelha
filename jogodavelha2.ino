//Variavel que verifica se ja tem ganhador
boolean jaGanhou;

boolean conseguiuJogar;
//char caracter;
//Matriz do jogo da velha
int matriz[3][3];
//Matriz de leds, tem 10 posições para não confundir a primeira posição(1) com 0
int posicaoLed[10][2];
//Array de led's para piscar quando houver ganhador
int ledGanhador[3];
//0 para jogador verde/ 1 para gorador vemelho
int numeroJogador = 0;
//contador de jogadas/ se for 9 quer dizer que deu velha
int contadorDeJogadas = 0;

//Pacote do receptor infravermelho
#include <IRremote.h>
//pino para leitura dos dados infravermelho
int RECV_PIN = 40;

IRrecv irrecv(RECV_PIN);

decode_results caracter;

void limpaMatriz(){
  //Limpa a matriz dizendo que ninguem jogou nela.
  //Colocando o valor -1 em todos os campos
  for(int i=0;i<3;i++){
    for(int j=0;j<3;j++){
      matriz[i][j] = -1;
    }
  }
}

void reiniciaJogo(){
  //Função para reiniciar o jogo.
  //Limpa a matriz
  limpaMatriz();
  //Apaga todos os Led's
  for(int i=1;i<10;i++){
    for(int j=0;j<2;j++){
      digitalWrite(posicaoLed[i][j], LOW);    
    }
  }
  //Reinicia as variaveis
  jaGanhou = false;
  numeroJogador = 0;
  contadorDeJogadas = 0;
}

void verificaGanhador(){
  //For para verificar se ganhou em linhas ou colunas
  for(int i=0;i<3;i++){
     //Verifica se ganhou em alguma das linhas
     if(matriz[i][0] == numeroJogador and matriz[i][1] == numeroJogador and matriz[i][2] == numeroJogador){
      //Calcula a posicao dos led's na matriz
      int pos = 3 * i;
      if(i == 0){
        pos = 0;
      }
      pos++;
      //guarda os led's que ganharam o jogo
      ledGanhador[0] = posicaoLed[pos][numeroJogador];
      ledGanhador[1] = posicaoLed[pos+1][numeroJogador];
      ledGanhador[2] = posicaoLed[pos+2][numeroJogador];
      //Informa que já tem um ganhador
      jaGanhou = true;
      Serial.print("jogador ganhou linha");  
      Serial.println();        
     }

     //Verifica se ganhou em alguma das colunas
     for(int i=0;i<3;i++){
       if(matriz[0][i] == numeroJogador and matriz[1][i] == numeroJogador and matriz[2][i] == numeroJogador){
        //Calcula a posicao dos led's na matriz
        int pos = i + 1;
        //guarda os led's que ganharam o jogo
        ledGanhador[0] = posicaoLed[pos][numeroJogador];
        ledGanhador[1] = posicaoLed[pos+3][numeroJogador];
        ledGanhador[2] = posicaoLed[pos+6][numeroJogador];
        //Informa que já tem um ganhador
        jaGanhou = true;
        Serial.print("jogardor ganhou coluna");  
        Serial.println();        
       }
    }
    //Verifica se ganhou nas diagonais
    if(matriz[0][0] == numeroJogador and matriz[1][1] == numeroJogador and matriz[2][2] == numeroJogador){
      //Informa que já tem um ganhador
      jaGanhou = true;
      //guarda os led's que ganharam o jogo
      ledGanhador[0] = posicaoLed[1][numeroJogador];
      ledGanhador[1] = posicaoLed[5][numeroJogador];
      ledGanhador[2] = posicaoLed[9][numeroJogador];
     }

     if(matriz[0][2] == numeroJogador and matriz[1][1] == numeroJogador and matriz[2][0] == numeroJogador){
      //Informa que já tem um ganhador
      jaGanhou = true;
      //guarda os led's que ganharam o jogo
      ledGanhador[0] = posicaoLed[3][numeroJogador];
      ledGanhador[1] = posicaoLed[5][numeroJogador];
      ledGanhador[2] = posicaoLed[7][numeroJogador];
     }
  }
}

void deuVelha(){
  //Se deu velha faz uma animacao com os led's
  //liga todos os led's
  for(int i=1;i<10;i++){
    for(int j=0;j<2;j++){
      digitalWrite(posicaoLed[i][j], HIGH);
      delay(100);
    }
  }
  //desliga todos os led's
  for(int i=1;i<10;i++){
    for(int j=0;j<2;j++){
      digitalWrite(posicaoLed[i][j], LOW);
      delay(100);
    }
  }
}

void jogarNaPosicao(int posicao){
    //função para jogar na posicao da matriz

    int linhaMatriz = 0;
    int colunaMatriz = 0;
    
    //Calcula a posição na matriz
    if (posicao < 4){
        linhaMatriz = 0;
        colunaMatriz = posicao-1;
    }else if(posicao > 3 and posicao < 7){
        linhaMatriz = 1;
        colunaMatriz = posicao-4;
    }else if (posicao > 6){
        linhaMatriz = 2;
        colunaMatriz = posicao-7;
    }
    //Fim do calculo
    
    //Verifica se ninguem jogou na posição
    if (matriz[linhaMatriz][colunaMatriz] == -1){
        //se ninguem jogou coloca o jogador na matriz e liga o led
        matriz[linhaMatriz][colunaMatriz] = numeroJogador;
        digitalWrite(posicaoLed[posicao][numeroJogador], HIGH);
        //informa que conseguiu jogar
        conseguiuJogar = true;
      }else{
        //informa que não conseguiu jogar
        Serial.print("Esta posicao ja foi jogada");  
        Serial.println();
      }
}

void setup() {  
  Serial.begin(9600);

  //INFRA VERMELHO
  Serial.println("Enabling IRin");
  irrecv.enableIRIn(); // Start the receiver
  Serial.println("Enabled IRin");
  
  //Inicia a variavel
  jaGanhou = false;

  //Inicia a matriz
  limpaMatriz();
  
  //Pinos correspondentes a cada posicao
  //A linha da matriz corresponde a posição no jogo
  //A coluna na matriz corresponde ao jogador
  //Posição 1 se jogador 0(verde) = led 3 senão jogador 1(vermelho) = led 4
  posicaoLed[1][0] = 3;
  posicaoLed[1][1] = 4;
  posicaoLed[2][0] = 5;
  posicaoLed[2][1] = 6;
  posicaoLed[3][0] = 7;
  posicaoLed[3][1] = 8;
  posicaoLed[4][0] = 9;
  posicaoLed[4][1] = 10;
  posicaoLed[5][0] = 11;
  posicaoLed[5][1] = 12;
  posicaoLed[6][0] = 30;
  posicaoLed[6][1] = 31;
  posicaoLed[7][0] = 32;
  posicaoLed[7][1] = 33;
  posicaoLed[8][0] = 34;
  posicaoLed[8][1] = 35;
  posicaoLed[9][0] = 36;
  posicaoLed[9][1] = 37;

  //Seta output de todos os led's
  for(int i=1;i<10;i++){
    for(int j=0;j<2;j++){
      pinMode(posicaoLed[i][j], OUTPUT);
    }
  }
}

void loop() {
  //Se jogou as 9 vezes e não ganhou, deu velha.
  if (contadorDeJogadas == 9 and !jaGanhou){
    deuVelha();
  }
  
  //Mostra quem ganhou
  if(jaGanhou){
    //apaga  os led's ganhadores
    for(int i = 0; i < 3; i++){
      digitalWrite(posicaoLed[ledGanhador[i]][numeroJogador], LOW);    
    }
    delay(100);  
    //ascende  os led's ganhadores
    for(int i = 0; i < 3; i++){
      digitalWrite(posicaoLed[ledGanhador[i]][numeroJogador], HIGH);    
    }
    delay(100);  
  }
  
  //Se o Serial está disponível
  if(Serial.available()){
    //caracter = Serial.read();
    //Serial.print(caracter);
    //Serial.println();

    //COMANDOS PASSADOR VIA CONTROLE REMOTO
    // 0x na frente para converter para hexadecimal
    //1 = 0xE12440BF
    //2 = 0xE12428D7
    //3 = 0xE1246897
    //4 = 0xE124B847
    //5 = 0xE124F00F
    //6 = 0xE1249867
    //7 = 0xE1247887
    //8 = 0xE124B04F
    //9 = 0xE124F807
    //0 = 0xE12430CF

    //Recebe os comandos via infravermelho
    if (irrecv.decode(&caracter)){
      //Mostra o que foi recebido via infravermelho
      Serial.println(caracter.value, HEX);
      irrecv.resume(); // Deixa o receptor infravermelho livre para receber novos dados

      //Quando Inicia ele não jogou
      conseguiuJogar = false;

      //Se o comando passado for 0 reinicia o jogo
      if(caracter.value == 0xE12430CF){
        //Começa novo jogo
        reiniciaJogo();
      }

      //Se não ganhou valida o que veio do infravermelho
      if(!jaGanhou){
        //Verifica se o que veio é uma posição valida
        //Se for valida tenta jogar na posicao informada
        switch (caracter.value) {
          case 0xE12440BF:
            jogarNaPosicao(1);
            break;
          case 0xE12428D7:
            jogarNaPosicao(2);
            break;
          case 0xE1246897:
            jogarNaPosicao(3);
            break;
          case 0xE124B847:
            jogarNaPosicao(4);
            break;
          case 0xE124F00F:
            jogarNaPosicao(5);
            break;
          case 0xE1249867:
            jogarNaPosicao(6);
            break;
          case 0xE1247887:
            jogarNaPosicao(7);
            break;
          case 0xE124B04F:
            jogarNaPosicao(8);
            break;
          case 0xE124F807:
            jogarNaPosicao(9);
            break;
        }

        //Verifica se conseguiu jogar (alimentado pela função: jogarNaPosicao )
        if(conseguiuJogar){
          //Verifica se tem ganhador
          verificaGanhador();
          //Marca mais uma jogada.
          contadorDeJogadas++;

          //Verifica se não ganhou para trocar o jogador
          if(!jaGanhou){
            //Se não ganhou troca o jogador
            if(numeroJogador == 1){
              numeroJogador = 0;
            }else{
              numeroJogador = 1;
            }  
          }
        }
      }
    }//FIM INFRAVERMELHO
  }//FIM SERIAL
}